<?php
$root = $_SERVER['DOCUMENT_ROOT'];
include $root.'/classes/SvsLibrary.php';
use classes\SvsLibrary;

if(SvsLibrary::isAjax()) {
	$full_name = @$_POST['name'];
	$pathinfo = pathinfo($full_name);
	$name = @$pathinfo['filename'];
	$ext = @$pathinfo['extension'];
	$success = true;
	$message = '';

	if(!$name || !$ext) {
		$success = false;
		$message = 'invalid file name';
	}

	if($success) {
		$path = SvsLibrary::getVipsTmpDir().'/'.$name.'.'.$ext;
		$svsFolder = @(SvsLibrary::getSettings())->svs_dir.$name.'/';
		@mkdir($svsFolder);
//		$cmd = 'VIPS_CONCURRENCY=1 vips dzsave '.$path.' '.$svsFolder.' --suffix .jpg --vips-progress > '.Config::$svsDir.'/'.$name.'/progress.txt';
//		$cmd = 'VIPS_CONCURRENCY=1 vips openslideload '.$path.' '.$svsFolder.$name.'.dz[angle=d90] --autocrop --vips-progress > '.Config::$svsDir.'/'.$name.'/progress.txt';
		$cmd = 'vips openslideload '.$path.' '.$svsFolder.$name.'.dz --autocrop --vips-progress > '.@(SvsLibrary::getSettings())->svs_dir.$name.'/progress.txt';

		$last_line = system($cmd, $retval);

		if(!SvsLibrary::folderExist($svsFolder)) {
			$success = false;
			$message = 'error processing script';
			@unlink($path);
		} else {
			@unlink($path);
		}
	}

	echo SvsLibrary::toJson(['success' => $success, 'message' => $message]);exit;
}