<?php
$root = $_SERVER['DOCUMENT_ROOT'];
$root_host = $root;
include $root.'/classes/SvsLibrary.php';
$root = dirname($root);

if (!file_exists($root.'/svs_options')) {
	mkdir($root.'/svs_options', 0777, true);
}

if (!file_exists($root.'/svs_library')) {
	mkdir($root.'/svs_library', 0777, true);
}

if (!file_exists($root.'/scripts')) {
	@mkdir($root.'/scripts', 0777, true);
}

if (!file_exists($root.'/scripts/vips')) {
	@mkdir($root.'/scripts/vips', 0777, true);
}

if (!file_exists($root.'/scripts/vips/tmp')) {
	@mkdir($root.'/scripts/vips/tmp', 0777, true);
}

if (!file_exists($root.'/svs_options/brush_colors.json')) {
	file_put_contents($root.'/svs_options/brush_colors.json', '["#7bd148","#5484ed","#a4bdfc","#46d6db"]');
}

if(!is_link($root_host.'/svs_library/')) {
	$target = $root.'/svs_library/';
	$link = $root_host.'/svs_library/';
//	$cmd = 'mklink /D "' . str_replace('/', '\\', $link) . '" "' . str_replace('/', '\\', $target) . '"';
	symlink($target, $link);
}
include 'brush.php';
?>
