<?php
$root = $_SERVER['DOCUMENT_ROOT'];
include $root.'/classes/SvsLibrary.php';
use classes\SvsLibrary;

if(SvsLibrary::isAjax()) {
	$svsName = @$_GET['svs_name'];

	$fname = $_FILES['xml_select_input_name']['name'];
	$content= file_get_contents($_FILES['xml_select_input_name']['tmp_name']);
	$success = true;
	$message = '';
	$brush_data = ['objects' => []];
	$not_accept_region_types = [2,3,4,5];

	$ext = pathinfo($fname, PATHINFO_EXTENSION);
	$filename = pathinfo($fname, PATHINFO_FILENAME);

	$ext_arr = ['xml'];

	if(!$svsName || !\classes\SvsLibrary::svsExists($svsName)) {
		$success = false;
		$message = 'undefined svs';
	}

	if (!in_array($ext, $ext_arr)) {
		echo SvsLibrary::toJson(['success' => false, 'message' => 'wrong extension']);
		exit;
	}

	libxml_use_internal_errors(true);
	$doc = simplexml_load_string($content);
	$xml = explode("\n", $content);

	if (!$doc) {
		$errors = libxml_get_errors();

		foreach ($errors as $error) {
			$success = false;
			$message = $error;
			break;
		}
	}

	if($success) {
		if($doc->Annotation) {
			foreach($doc->Annotation as $annotation) {
				$OLE_color = (int)($annotation['LineColor']);

				$blu = SvsLibrary::toString16((($OLE_color / 65536) % 256));
				$grn = SvsLibrary::toString16((string)(($OLE_color / 256) % 256));
				$red = SvsLibrary::toString16((string)($OLE_color % 256));

				if (strlen($blu) < 2) { $blu = "0".$blu; }
				if (strlen($grn) < 2) { $grn = "0".$grn; }
				if (strlen($red) < 2) { $red = "0".$red; }
				$rgn = $red.$grn.$blu;

				if($annotation->Regions && count($annotation->Regions->Region) && count($annotation->Regions->Region)) {
					foreach($annotation->Regions->Region as $region) {
						$region_type = (int)($region['Type']);

						if(in_array($region_type, $not_accept_region_types)) {
							continue;
						}

						if($region->Vertices && count($region->Vertices->Vertex)) {
							@$brush_data['objects'][] = [
								"type" => "polygon",
								"originX" => "left",
								"originY" => "top",
								"fill" => "#".$rgn,
								"strokeWidth" => 1,
								"points" => [],
							];
							$key_object = count($brush_data['objects']) - 1;

							foreach($region->Vertices->Vertex as $vertex) {
								@$brush_data['objects'][$key_object]['points'][] = [
									'x' => (float)$vertex['X'],
									'y' => (float)$vertex['Y'],
								];
							}
						}
					}
				}
			}
		}

		$brush_filename = SvsLibrary::saveBrush($svsName, $filename, json_encode($brush_data));
		if(!$brush_filename) {
			$success = false;
			$message = 'error while saving file';
		}
	}

	if(!$success) {
		echo SvsLibrary::toJson(['success' => $success, 'message' => $message]);exit;
	}

	exit(json_encode(array('success' => true, 'name' => $brush_filename)));
}