<?php
$root = $_SERVER['DOCUMENT_ROOT'];
include $root.'/classes/SvsLibrary.php';
set_time_limit(0);
ini_set('max_input_time', '0');
ini_set('max_execution_time', '-1');
ini_set('memory_limit', '-1');

if(\classes\SvsLibrary::isAjax()) {
	$fname = $_FILES['download_select_input_name']['name'];
	$fsize = $_FILES['download_select_input_name']['size'];
	$free_space = \classes\SvsLibrary::getFreePlaceOnDisc();
	$message = '';

	$ext = pathinfo($fname, PATHINFO_EXTENSION);

	$ext_arr = ['svs', 'scn'];

	if (!in_array($ext, $ext_arr)) {
		echo \classes\SvsLibrary::toJson(['success' => false, 'message' => 'wrong extension']);
		exit;
	}

	if ($free_space < $fsize) {
		exit(json_encode(['result' => false,
			'size' => false,
			'delete_size' => \classes\SvsLibrary::isa_convert_bytes_to_specified(($fsize - $free_space), 'G', 2),
		]));
	}
	@mkdir(\classes\SvsLibrary::getVipsTmpDir(), 0777);

	$svs_name = pathinfo($fname, PATHINFO_FILENAME);
	$path = \classes\SvsLibrary::getVipsTmpDir() . '/' . $fname;
	$k = 1;
	$name = $svs_name;
	do {
		if (\classes\SvsLibrary::folderExist(@(\classes\SvsLibrary::getSettings())->svs_dir.$name)) {
			$name = $svs_name . '_' . $k;
			$path = \classes\SvsLibrary::getVipsTmpDir() . '/' . $name . '.' . $ext;
		} else {
			break;
		}

		$k++;
	} while (true);

	$success = true;

	if (move_uploaded_file($_FILES['download_select_input_name']['tmp_name'], $path) && file_exists($path)) {
		echo \classes\SvsLibrary::toJson(['success' => true, 'full_name' => $name . '.' . $ext, 'name' => $name]);exit;
	} else {
		$success = false;
		$message = 'error file upload';
	}

	if(!$success) {
		echo \classes\SvsLibrary::toJson(['success' => $success, 'message' => $message]);exit;
	}

	exit(json_encode(array('success' => false, 'message' => 'error')));
}