<?php
$root = $_SERVER['DOCUMENT_ROOT'];
include $root.'/classes/SvsLibrary.php';
use classes\SvsLibrary;

if(SvsLibrary::isAjax()) {
	$svs_name = @$_POST['svs_name'];
	$fabricName = @$_POST['fabric_name'];
	$data = @$_POST['data'];
	$data_send = [];

	if($data && $svs_name) {
		$svs_dir = SvsLibrary::getSvsDir().'/'.$svs_name;
		$brush_dir = $svs_dir.'/brush';
		$file = $brush_dir.'/'.$fabricName.'.json';

		if(!$fabricName || strlen($fabricName) > 64) {
			exit(SvsLibrary::ajaxJson([
				'success' => false,
				'message' => (strlen($fabricName) > 64 ? 'Название рисовки не должно превышать 64 символов.' : 'Не указано имя рисовки.')
			]));
		}

		if(SvsLibrary::folderExist($svs_dir)) {
			if (!is_dir($brush_dir)) {
				mkdir($brush_dir);
			}

			if(file_put_contents($file, $data)) {
				$data_send = ['success' => true, 'message' => 'Изменения сохранены.'];
			} else {
				$data_send = ['success' => false, 'message' => 'Запись в файл не произошел.'];
			}
		} else {
			$data_send = ['success' => false, 'message' => 'Такой svs не существует.'];
		}
	} else {
		$data_send = ['success' => false, 'message' => 'Переданы не верные данные.'];
	}

	$data_send['message'] = $fabricName.': '.$data_send['message'];

	exit(SvsLibrary::ajaxJson($data_send));
} else {
	exit(404);
}