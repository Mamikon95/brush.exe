<!doctype html>
<html>
<head>
	<meta charset="UTF-8">
	<link href='/css/bootstrap.min.css' rel='stylesheet' type='text/css'>
	<link href='/css/choose_settings.css' rel='stylesheet' type='text/css'>
	<title>Settings</title>
</head>
<body>
<form action="" method="post" enctype="multipart/form-data">
	<div class="form-group">
		<div class="input-group input-file">
			<span class="input-group-btn">
        		<button class="btn btn-default btn-choose" type="button" id="open_dir_input">Открыть</button>
    		</span>
			<input type="text" class="form-control" id="dir_input" placeholder='Выберите директорию' />
		</div>
		<input type="submit" class="btn btn-default">
	</div>
</form>

<script src='https://code.jquery.com/jquery-1.11.0.min.js' integrity="sha256-spTpc4lvj4dOkKjrGokIrHkJgNA0xMS98Pw9N7ir9oI=" crossorigin="anonymous"></script>
<script>
	function bs_input_file() {
		$(".input-file").before(
			function() {
				if ( ! $(this).prev().hasClass('input-ghost') ) {
					var element = $("<input type='file' class='input-ghost' name='svs_dir' style='visibility:hidden; height:0' webkitdirectory mozdirectory msdirectory odirectory directory>");
					element.attr("name",$(this).attr("name"));
					element.change(function(e) {
						getFolder(e);
						element.next(element).find('input').val((element.val()).split('\\').pop());
					});
					$(this).find("button.btn-choose").click(function(){
						element.click();
					});
					$(this).find('input').css("cursor","pointer");
					$(this).find('input').mousedown(function() {
						$(this).parents('.input-file').prev().click();
						return false;
					});

					function getFolder(e) {
						var files = e.target.files;
						console.log(e.target);
						var path = files[0].webkitRelativePath;
						var Folder = path.split("/");
						// alert(Folder[0]);
					}

					return element;
				}
			}
		);
	}
	$(function() {
		bs_input_file();
	});
</script>
</body>
</html>