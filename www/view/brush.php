<?php
$root = $_SERVER['DOCUMENT_ROOT'];
$svs_dir = \classes\SvsLibrary::getSvsDir();
$svs_name = @$_GET['svs_name'];
$fabric_name = @$_GET['fabric_name'];
$colors = \classes\SvsLibrary::getBrushColors();
$fabrics = \classes\SvsLibrary::getAllFabrics($svs_name);
$libFolder = \classes\SvsLibrary::getLibFolders();
if(!$svs_name) {
	$svs_name = current(array_keys($libFolder));
}
$model_selected = @$_GET['model'];
$confiPyr = \classes\SvsLibrary::getCreatedSvsPyramidConfig($svs_name);
$confiPyrPbi = \classes\SvsLibrary::getCreatedSvsPyramidConfig($svs_name,$model_selected);
$img_size = (int)@$confiPyr->size;
$width_len = $img_size * ((int)@$confiPyr->x);
$height_len = $img_size * ((int)@$confiPyr->y);
$widthP_len = $img_size * ((int)@$confiPyrPbi->x);
$heightP_len = $img_size * ((int)@$confiPyrPbi->y);
$maxLevelPrbi = ((int)@$confiPyrPbi->maxLevel);
$maxLevel = (int)@$confiPyr->maxLevel;
$fabric_names = $fabric_name ? explode(',', $fabric_name) : [];
$fabric_names = array_slice($fabric_names, 0, 4);
$fabric_jsons = [];

foreach($fabric_names as $key => $fn) {
	$file = $svs_dir.'/'.$svs_name.'/brush/'.$fn.'.json';
	if(file_exists($file)) {
		$fabric_jsons[$fn] = file_get_contents($file);
	} else {
		unset($fabric_names[$key]);
	}
}

$isNew = (string)!count($fabric_jsons);
$fabric_jsons_js = json_encode($fabric_jsons);
$fabric_jsons_count = count($fabric_jsons);

$svsModels = \classes\SvsLibrary::getSvsModels($svs_name);
array_unshift($svsModels,'');

$dziFile = '/svs_library/'.$svs_name.'/'.$svs_name.'.dzi';

if(!is_file($root.$dziFile)) {
	$dziFile = '';
}

if($dziFile) {
	$dziFileContent = simplexml_load_file($root.$dziFile);
	$width_len = (string)$dziFileContent->Size['Width'];
	$height_len = (string)$dziFileContent->Size['Height'];
}
?>
<!doctype html>
<html>
<head>
	<meta charset='utf-8'>
	<title><?=$svs_name?> - Рисование на деконве</title>
<!--	<link href='/css/style_viewer_deconv.css?v=1.1' rel='stylesheet' type='text/css' media='all'>-->
	<link href='/css/bootstrap.min.css' rel='stylesheet' type='text/css'>
	<link href='/css/brush.css?v=2.3' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="/css/jquery.growl.css" type="text/css">
	<link rel="stylesheet" href="/css/font-awesome.min.css" type="text/css">
	<link rel="stylesheet" href="/css/plunk/jquery.simplecolorpicker.css" type="text/css">
	<link rel="stylesheet" href="/css/select2/select2.min.css" type="text/css">
</head>
<body id='main'>
<div class='page_container'>
	<!--	<canvas id="viewer-sketchpad"></canvas>-->
	<div id='viewer-container'>
		<div class="row">
			<div id="viewer" class="viewer"></div>
		</div>
		<div class="sketch-right-options panel">
			<?php require_once $root.'/view/_parts/select_svs.php'?>
			<h4>
				<Span>Svs:</Span>
			</h4>
			<div>
				<p>Upload svs</p>
				<form id='download-form' method="POST" enctype="multipart/form-data">
					<input type="file" accept=".svs,.scn" id="download-select-input" name="download_select_input_name" class="hidden">
					<input type="hidden" name="MAX_FILE_SIZE" value="300000000" />
				</form>
				<p class="btn btn-primary col-xs-12" id="download-select-file">Select file</p>
			</div>
			<div class="download-xml-file-div">
				<h4>
					<Span>Xml:</Span>
				</h4>
				<div>
					<p>Import xml file</p>
					<form id='download-xml-form' method="POST">
						<input type="file" accept=".xml" id="download-select-xml-input" name="xml_select_input_name" class="hidden">
					</form>
					<p class="btn btn-primary col-xs-12" id="download-select-xml-file">Select file</p>
				</div>
				<div class="export-xml-div">
					<p class="btn btn-success col-xs-12" id="export-xml-bt">Export xml file</p>
				</div>
			</div>
			<h4>
				<span>Image:</span>
			</h4>
			<div id="layers-list"></div>
			<div class="sketch-draw">
				<label for="select-fabric">Manual labelings stored:</label>
				<select name="select-fabric[]" id="select-fabric" class="form-control" multiple="multiple">
					<?php foreach($fabrics as $fabric):?>
						<option value="<?=$fabric?>" <?=in_array($fabric, $fabric_names) ? 'selected="selected"' : ''?>><?=$fabric?></option>
					<?php endforeach;?>
				</select>

				<label for="model">Programed labeling select:</label>
				<select id='select-model' class='form-control' name="model">
					<?php foreach($svsModels as $model):?>
						<option value="<?=$model?>" <?=$model_selected === $model ? 'selected="selected"' : ''?>><?=$model?></option>
					<?php endforeach;?>
				</select>
				<input type="button" class="btn gold-btn" value="Update" id="save-fabric-select">
			</div>
			<div class="options-func-item">
				<label for="erase-mode"><span class="fa fa-eraser"></span> <span class="label-text">Eraser Mode</span></label>
				<input type="checkbox" name="erase-mode" id="erase-mode"> <br>
				<span class="signature">e - On / Off the eraser</span>
			</div>
			<div class="options-func-item">
				<label for="poly-mode"><span class="fa fa-square"></span> <span class="label-text">Polygon Mode</span></label>
				<input type="checkbox" name="poly-mode" id="poly-mode"><br>
				<span class="signature">"Enter" - Формировать</span>
			</div>
			<hr>
			<label for="color-picker">I - next colour</label><br>
			<select name="color-picker" id="color-picker">
				<?php foreach($colors as $color):?>
					<option value="<?=$color?>"><?=$color?></option>
				<?php endforeach;?>
			</select>
			<hr>
			<div>
				<div>
					<div class="slider-wrapper">
						<label for="size-picker">Size</label><br>
						<input type="range" min="1" max="300" value="100" step="1" id="size-picker-range" >
						<output name="sprOutput" id="size-picker"></output>
					</div>
				</div>
				<div>
					<div class="slider-wrapper">
						<label for="scroll-picker">Scrolling speed</label><br>
						<input type="range" min="50" max="200" value="70" step="1" id="scroll-picker-range" >
						<output name="scrollOutput" id="scroll-picker"></output>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="urs-block">
		<div class="ur-block">
			<button id="undo-bt" class="btn btn-ur">Undo</button>
			<button id="redo-bt" class="btn btn-ur">Redo</button>
			<div class="viewer-container"></div>
		</div>
		<div class="sketch-options">
			<div class="so-new-view">
				<?php if($isNew):?>
					<label for="fabric_name">Fill to save with new name:</label>
					<input type="text" class="form-control" name="fabric_name" id="fabric_name" placeholder="" value="<?=$fabric_name?>">
				<?php endif;?>
			</div>
			<?php if(!@$confiPyr->isCreated && !$dziFile):?>
				<div class="warning_div">
					<h3>Folders with images of pyramids are not formed for viewing!</h3>
				</div>
			<?php endif;?>
			<div class="so-options">
				<input type="button" id="sketch-save" value="Save" class="btn gold-btn">
				<input type="button" value="Clear labeling" class="btn gold-btn so-start-new-bt" onclick="location.href='/view/index.php?svs_name=<?=$svs_name?>'">
			</div>
		</div>
	</div>
</div>

<script src="/js/jquery-3.3.1.min.js"></script>
<script src='/js/openseadragon.js'></script>
<script src='/js/fabric/openseadragon-fabricjs-overlay.js'></script>
<script src='/js/fabric/fabric.adapted.js'></script>
<script src='/js/bootstrap.min.js'></script>
<script src='/js/sketchpad.js'></script>
<script src='/js/cookie/jquery.cookie.js'></script>
<script src='/js/jquery.growl.js'></script>
<script src='/js/plunk/jquery.simplecolorpicker.js'></script>
<script src='/js/select2/select2.min.js'></script>
<script src='/js/jquerysortable/jquery-sortable.js'></script>
<script src='/js/loadingoverlay.min.js'></script>
<script src='/js/brush.js?v=4.0'></script>

<script>
<?php
$js = <<<JS
$(document).ready(function() {
		App.init('viewer' ,$height_len, $width_len, $img_size, '$svs_dir', '$svs_name', $fabric_jsons_js, '$isNew', '$maxLevel', '$widthP_len', '$heightP_len', '$maxLevelPrbi', '$model_selected', '$dziFile',
			{
				'color' : '#color-picker',
				'size' : '#size-picker',
				'size_range' : '#size-picker-range',
				'scroll' : '#scroll-picker',
				'scroll_range' : '#scroll-picker-range',
				'undo' : '#undo-bt',
				'redo' : '#redo-bt',
				'erase_checkbox' : '#erase-mode',
				'poly_checkbox' : '#poly-mode',
				'save_bt' : '#sketch-save',
				'fabric_name' : '#fabric_name',
				'fabrics_select' : '#select-fabric',
				'model_select' : '#select-model',
				'fabric_append_select' : '#save-fabric-select',
				'current_view_fabric' : '#current-fabric',
				'svs_select' : '#select-svs',
				'viewer_container' : '.viewer-container',
				'layers_list' : '#layers-list',
				'sketch_options_panel' : '.sketch-right-options',
				'undo_redo_select_block' : '.urs-block',
				'select_download_file_bt' : '#download-select-file',
				'select_download_file_form' : '#download-form',
				'select_download_file_input' : '#download-select-input',
				'select_download_xml_file_bt' : '#download-select-xml-file',
				'select_download_xml_file_form' : '#download-xml-form',
				'select_download_xml_file_input' : '#download-select-xml-input',
				'export_xml_bt' : '#export-xml-bt'
			},
			{
				'save_url' : '/view/save_json.php',
				'page_url' : '/view/index.php',
				'svs_images_request_path' : '/view/get_svs_images.php',
				'export_xml_url' : '/view/export_xml.php'
			}
		);
	})
JS;
echo $js;
?>
</script>
</body>
</html>