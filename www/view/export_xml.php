<?php
$root = $_SERVER['DOCUMENT_ROOT'];
include $root.'/classes/SvsLibrary.php';
use classes\SvsLibrary;

if(SvsLibrary::isAjax()) {
	$svs_name = @$_POST['svs_name'];
	$fabric_name = @$_POST['fabric_name'];
	$fabric_json_text = @$_POST['fabric_json'];
	$success = true;

	if(!\classes\SvsLibrary::svsExists($svs_name)) {
		$success = false;
		$message = 'svs does not exist';
	}

	if(!SvsLibrary::isJson($fabric_json_text)) {
		$success = false;
		$message = 'invalid image json';
	}

	if(!$fabric_name) {
		$fabric_name = uniqid();
	}

	$fabric_json = json_decode($fabric_json_text);
	$fabric_json_to_xml_arr = [];

	foreach($fabric_json->objects as $object) {
		if($object->type == 'polygon') {
			@$fabric_json_to_xml_arr[$object->fill][] = $object->points;
		}
	}


	$xml = new SimpleXMLElement('<Annotations/>');
	$annotations = $xml;

	$annotation_id = 1;
	foreach($fabric_json_to_xml_arr as $fill => $objects) {
		$rgb = str_replace('#', '', $fill);
		if(strlen($rgb) < 6) {
			$ole = 0;
		} else {
			$red = $rgb[0].$rgb[1];
			$grn = $rgb[2].$rgb[3];
			$blu = $rgb[4].$rgb[5];
			$ole = intval($red, 16) + (intval($grn, 16) * 256) + (intval($blu, 16) * 256 * 256);
		}

		$annotation = $annotations->addChild('Annotation');
		$annotation->addAttribute('Id', $annotation_id);
		$annotation->addAttribute('Type', 4);
		$annotation->addAttribute('LineColor', $ole);
		$annotation->addAttribute('ReadOnly', 0);
		$annotation->addAttribute('NameReadOnly', 0);
		$annotation->addAttribute('LineColorReadOnly', 0);
		$annotation->addAttribute('Incremental', 0);
		$annotation->addAttribute('Visible', 1);
		$annotation->addAttribute('Selected', 0);
		$annotation->addAttribute('MarkupImagePath', '');
		$annotation->addAttribute('MacroName', '');

		$annotation->addChild('Attributes');

		$regions = $annotation->addChild('Regions');
		$region_id = 1;
		$regions->addChild('RegionAttributeHeaders');

		if(count($objects)) {
			foreach($objects as $object) {
				if(count($object)) {
					$region = $regions->addChild('Region');
					$region->addChild('Attributes');
					$vertices = $region->addChild('Vertices');
					$region->addAttribute('Id', $region_id);
					$region->addAttribute('Type', 1);
					$region->addAttribute('Selected', 0);
					$region->addAttribute('DisplayId', $region_id);

					foreach($object as $point) {
						$vertex = $vertices->addChild('Vertex');
						$vertex->addAttribute('X', $point->x);
						$vertex->addAttribute('Y', $point->y);
						$vertex->addAttribute('Z', 0);
					}
					$region_id++;
				}
			}
		}
		$annotation_id++;
	}

	if(!$success) {
		echo SvsLibrary::toJson(['success' => $success, 'message' => $message]);exit;
	}

	echo SvsLibrary::toJson(['success' => $success, 'name' => $fabric_name, 'xml' => $xml->asXML()]);exit;
}