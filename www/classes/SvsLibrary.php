<?php
namespace classes;

class SvsLibrary {

	const LIB_FOLDER = '/svs_library';
	const IMG_SIZES = 2000;
	const IMG_TYPE = 'jpg';
	protected static $session;
	const PYRAMID_CONFIG_FILE = 'pyramid_config';
	const SVS_MODELS_FILE = 'models.json';

	public static function getLibFolders($size_unit = '', $size_original = false) {
		$folders = glob(self::getSvsDir() . '/*' , GLOB_ONLYDIR);

		$data = [];

		if(is_array($folders)) {
			foreach($folders as $folder) {
				$folder_name = ($f_exp = explode('/', $folder))[count($f_exp) - 1];
				$url = '/svs_library/'.$folder_name;
				$folder_path = $folder;
				if($size_original) {
					$folder_path = $folder.'/'.$folder_name.'_s1_2000_imgs';
				}

				if(is_dir($folder_path)) {
					$data[$folder_name] = [
						'url' => $url,
					];
				}
			}
		}

		return $data;
	}

	public static function printSvsImage($img_path) {
		$image = @imagecreatefrompng($img_path);
		if($image) {
			$bg = imagecreatetruecolor(imagesx($image), imagesy($image));
			imagefill($bg, 0, 0, imagecolorallocate($bg, 255, 255, 255));
			imagealphablending($bg, TRUE);
			imagecopy($bg, $image, 0, 0, 0, 0, imagesx($image), imagesy($image));
			imagedestroy($image);
			$quality = 50; // 0 = worst / smaller file, 100 = better / bigger file
			$timer = time() + (24 * 60 * 60 * 7);
			$stime_e = date('D, d M Y H:i:s', $timer);
			$stime_e2 = $stime_e . ' GMT';
			$timer2 = time() - (24 * 60 * 60);
			$stime_e3 = date('D, d M Y H:i:s', $timer2);
			$stime_e5 = $stime_e3 . ' GMT';
			header ('Content-Type: image/png');
			header ('expires: '.$stime_e2);
			header ('Last-Modified: '.$stime_e5);
			imagejpeg($bg, NULL, $quality);
			imagedestroy($bg);
		} else {
			exit(404);
		}
	}

	public static function getFreePlaceOnDisc() {
		$dfs = disk_free_space(str_replace('\\', '/', dirname($_SERVER['DOCUMENT_ROOT']))); // Свободное место на диске

		return $dfs;
	}

	public static function getRoot() {
		return str_replace('\\', '/', dirname($_SERVER['DOCUMENT_ROOT']));
	}

	public static function isGuestMode() {
		self::$session = !self::$session ? new \Session() : self::$session;
		$SHSC = self::getSvsHSC();
		$mode_password = self::$session->get('password');

		return $SHSC->mode_password !== $mode_password;
	}

	public static function svsExists($svs_name) {
		$svs_dir = array_map('basename', glob(@(self::getSettings())->svs_dir.'*', GLOB_ONLYDIR));
		return in_array($svs_name, $svs_dir);
	}

	public static function toString16($number) {
		return dechex($number);
	}

	public static function saveBrush($svs_name, $brush_name, $json_data) {
		if(!$svs_name || !$brush_name || !$json_data) {
			return false;
		}

		$svs_brush_dir = @(self::getSettings())->svs_dir.$svs_name.'/brush';
		@mkdir($svs_brush_dir);

		$k = 1;
		$tmp_name = $brush_name;

		while(file_exists($svs_brush_dir.'/'.$brush_name.'.json')) {
			$brush_name = $tmp_name.'_'.$k;
			$k++;
		}

		if(file_put_contents($svs_brush_dir.'/'.$brush_name.'.json', $json_data)) {
			return $brush_name;
		} else {
			return false;
		}
	}

	public static function getSettings() {
		$settings = json_encode([
			'svs_dir' => str_replace('\\', '/', dirname($_SERVER['DOCUMENT_ROOT']).'/svs_library/'),
			'svs_options_dir' => str_replace('\\', '/', dirname($_SERVER['DOCUMENT_ROOT']).'/svs_options/')
		]);
		return json_decode($settings);
	}

	public static function getSvsDir() {
		return @(self::getSettings())->svs_dir;
	}

	public static function getBrushColors() {
		$data = file_get_contents((@(self::getSettings())->svs_options_dir).'/brush_colors.json');
		return json_decode($data);

	}

	public static function getAllFabrics($svs_name) {
		$dir = self::getSvsDir().'/'.$svs_name.'/brush';

		$fabrics = array_map('basename', glob($dir.'/*.json'));
		$fabrics = array_map(function($fabric) {
			return pathinfo($fabric, PATHINFO_FILENAME);
		}, $fabrics);

		return $fabrics;
	}

	public static function isAjax() {
		return !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest';
	}

	public static function ajaxJson($data) {
		header('Content-Type: application/json');
		return json_encode($data);
	}

	public static function folderExist($folder)
	{
		$path = realpath($folder);
		return ($path !== false AND is_dir($path)) ? true : false;
	}

	public static function toJson($data) {
		header('Content-Type: application/json');
		return json_encode($data);
	}

	public static function isa_convert_bytes_to_specified($bytes, $to, $decimal_places = 1) {
		$formulas = array(
			'K' => number_format($bytes / 1024, $decimal_places),
			'M' => number_format($bytes / 1048576, $decimal_places),
			'G' => number_format($bytes / 1073741824, $decimal_places)
		);
		return isset($formulas[$to]) ? $formulas[$to] : 0;
	}

	public static function getCreatedSvsPyramidConfig($svs_name, $model = 'imgs') {
		$svs_path = self::getSvsDir().'/'.$svs_name;
		$tmp_path = $svs_path.'/tmp';
		$config_path = $tmp_path.'/'.self::PYRAMID_CONFIG_FILE.'_'.$model.'.json';

		if(file_exists($config_path)) {
			$config = file_get_contents($config_path);
			return self::isJson($config) ? json_decode($config) : json_encode([]);
		}

		return json_encode([]);
	}

	public static function isJson($string) {
		json_decode($string);
		return (json_last_error() == JSON_ERROR_NONE);
	}

	public static function getSvsModels($svs_name, $only_models = true) {
		$file = self::getSvsDir().'/'.$svs_name.'/'.self::SVS_MODELS_FILE;
		$data = [];
		if(file_exists($file)) {
			$json_sm = file_get_contents($file);
			$json_arr = json_decode($json_sm,true);
			if($only_models) {
				$json_arr = array_keys($json_arr);

				foreach($json_arr as $item) {
					if($item != 'imgs') {
						$data[] = $item;
					}
				}
			} else {
				$data = $json_arr;
			}
		}

		return $data;
	}

	public static function getVipsTmpDir() {
		return self::getRoot().'/scripts/vips/tmp';
	}
}