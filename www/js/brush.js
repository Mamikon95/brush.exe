let js_addr = 'http://antshealth.tech/js/';
let css_addr = 'http://antshealth.tech/css/';
App = {
	fabrics: [],
	init: function(viewerId, height_len, width_len, img_size, svs_dir, svs_name, fabric_jsons, isNewFabric, maxLevel, widthP_len, heightP_len, maxLevelP, model_selected, dziFile, selectors, urls) {
		let self = this;
		let eraseMode = false;
		let polyMode = false;
		this.selectors = selectors;
		this.svs_name = svs_name;
		this.urls = urls;
		this.viewerId = viewerId;
		this.brushMinSize = 1;
		this.brushMaxSize = 300;
		if(model_selected) {
			this.viewerIdP = 'slideP';
			this.viewerEmpty = 'emptyV';
			$('#' + viewerId).before($('<div id="' + this.viewerEmpty + '" class="viewer viewerP"></div>'));
			$('#' + viewerId).before($('<div id="' + this.viewerIdP + '" class="viewer viewerP"></div>'));
		}

		this.showViewElements();
		this.setViewElementsProperties();
		this.getSimpleColor();

		const duomo = {
			height: height_len,
			width:  width_len,
			tileSize: img_size,
			defaultZoomLevel: 0,
			minLevel: 0,
			maxLevel: maxLevel,

			wrapHorizontal: false,
			getTileUrl: function (level, x, y) {
				let start_y = 1;
				let start_x = 1;

				return svs_dir + level + '_imgs/' + (img_size * y + start_y)  + "_" + (img_size * x + start_x) +".png";
			}
		};

		this.viewerOptions = {
			scale: width_len
		};

		if(model_selected) {
			const duomo2 = {
				height: parseInt(heightP_len),
				width:  parseInt(widthP_len),
				tileSize: parseInt(img_size),
				defaultZoomLevel: 0,
				minLevel: 0,
				maxLevel: parseInt(maxLevelP),

				wrapHorizontal: false,
				getTileUrl: function (level, x, y) {
					let start_y = 1;
					let start_x = 1;

					return svs_dir + level + '_' + model_selected + '/' + (img_size * y + start_y)  + "_" + (img_size * x + start_x) +".png";
				}
			};

			const duomo_empty = {
				height: height_len,
				width:  width_len,
				tileSize: img_size,
				defaultZoomLevel: 0,
				minLevel: 0,
				maxLevel: maxLevel,
				wrapHorizontal: false,
				getTileUrl: function (level, x, y) {
					let start_y = 1;
					let start_x = 1;

					return "null.png";
				}
			};

			this.viewer = OpenSeadragon({
				id: this.viewerEmpty,

				showNavigator: true,
				navigatorPosition: "BOTTOM_LEFT",

				tileSources: duomo_empty,
				smoothTileEdgesMinZoom: 1,
				animationTime: 0.2,
				opacity: 0.5,
			});


			this.viewer1 = OpenSeadragon({
				id: this.viewerIdP,

				showNavigator: true,
				navigatorPosition: "BOTTOM_LEFT",

				tileSources: duomo2,
				smoothTileEdgesMinZoom: 1,
				animationTime: 0.2,
				opacity: 0.5,
			});

			this.viewer2 = OpenSeadragon({
				id: viewerId,

				showNavigator: true,
				navigatorPosition: "BOTTOM_LEFT",

				tileSources: duomo,
				smoothTileEdgesMinZoom: 1,
				animationTime: 0.2,
				opacity: 1,
			});

			var viewer1Handler = function() {
				self.viewer2.viewport.zoomTo(self.viewer.viewport.getZoom());
				self.viewer2.viewport.panTo(self.viewer.viewport.getCenter());
				self.viewer1.viewport.zoomTo(self.viewer.viewport.getZoom());
				self.viewer1.viewport.panTo(self.viewer.viewport.getCenter());
			};

			this.viewer.addHandler('zoom', viewer1Handler);
			this.viewer.addHandler('pan', viewer1Handler);

			let viewerIdP = this.viewerIdP;
			$(window).on('keyup', function(event) {
				if(event.keyCode === 90) {
					let $ob = $('#' + viewerIdP);
					if($ob.is(':hidden')) {
						$ob.show();
					} else {
						$ob.hide();
					}
				}
			})
		} else {
			this.viewer = OpenSeadragon({
				id: this.viewerId,

				showNavigator: true,
				navigatorPosition: "BOTTOM_LEFT",

				tileSources: (dziFile ? dziFile : duomo),
				smoothTileEdgesMinZoom: 1,
				animationTime: 0.2,
				opacity: 1,
			});
		}

		let overlays = {};
		let iter = 0;
		$.each(fabric_jsons, function(key, fabric_json) {
			overlays[key] = self.layer.addLayer(key, fabric_json);
		});

		if(isNewFabric) {
			self.layer.newLayer();
		} else {
			self.layer.showLayerCheckboxes();
		}

		self.layer.resize();

		let currentLayerOverlay = self.layer.currentLayerOverlay;

		let drag;

		this.MouseTracker = new OpenSeadragon.MouseTracker({
			element: self.viewer.container,
			nonPrimaryPressHandler: function(event) {
				if (event.button === 2) {
					drag = {
						lastPos: event.position.clone()
					};
				}
			},
			moveHandler: function(event) {
				if (drag) {
					let deltaPixels = drag.lastPos.minus(event.position);
					let deltaPoints = self.viewer.viewport.deltaPointsFromPixels(deltaPixels);
					self.viewer.viewport.panBy(deltaPoints);
					drag.lastPos = event.position.clone();
				}
			},
			nonPrimaryReleaseHandler: function(event) {
				if (event.button === 2) {
					drag = null;
				}
			}
		}).setTracking(true);

		$(selectors.color + ', ' + selectors.size).on('change', function () {
			setSketchOptions();
		});

		let isRedoing = false;
		let h = {};
		let mouseCursor = null;

		if(!isNewFabric) {
			self.layer.updateParams();
		}

		let fabricCanvas = null;
		let mouseLastPosition = null;
		function updateNewOverlayCanvas() {
			fabricCanvas = currentLayerOverlay.fabricCanvas();
			fabricCanvas.freeDrawingCursor = 'none';

			let isDrawing = false;
			mouseLastPosition = null;
			let isShiftPressed = false;
			let line = null;

			fabricCanvas.on('mouse:down', function(opt) {
				let pointer2 = fabricCanvas.getPointer(opt.e);

				if(isShiftPressed && mouseLastPosition && !polyMode) {
					let pathCoor = 'M ' + mouseLastPosition.x + ' ' + mouseLastPosition.y + ' L ' + pointer2.x + ' ' + pointer2.y;
					line = new fabric.Path(pathCoor, {
						strokeWidth: width,
						stroke: color,
						originX: 'center',
						originY: 'center',
					});
					polyObject(line);
					fabricCanvas.add(line);
				}

				isDrawing = true;
			});

			fabricCanvas.on('mouse:up', function(opt) {
				let pointer = fabricCanvas.getPointer(opt.e);
				mouseLastPosition = {x: pointer.x, y: pointer.y};
				isDrawing = false;
			});

			fabricCanvas.on('path:created', function (opt) {
				if(line && isShiftPressed) {
					let object = opt.path;
					let lineObj = line;
					object.setStroke($(selectors.color).val());
					lineObj.setStroke($(selectors.color).val());
					groupObjects(lineObj, object);
				}
			});

			let groupObjects = function(obj1, obj2) {
				let group = new fabric.Group();
				fabricCanvas.add(group);
				group.add(obj1);
				group.add(obj2);
				group.addWithUpdate();
				obj1.remove();
				obj2.remove();
				group.setCoords();
				fabricCanvas.renderAll();
			};

			mouseCursor = {
				mousecursor: new fabric.Circle({
					left: 0,
					top: 0,
					radius: fabricCanvas.freeDrawingBrush.width / 2,
					fill: fabricCanvas.freeDrawingBrush.color,
					originX: 'center',
					originY: 'center',
				}),
				cursorIn: true,
				init: function() {
					let self = this;
					fabricCanvas.add(this.mousecursor);

					fabricCanvas.on('mouse:move', function (obj) {
						if(!self.cursorIn && !polyMode) {
							self.cursorIn = true;
							fabricCanvas.add(self.mousecursor);
						}

						if(!polyMode) {
							let mouse = fabricCanvas.getPointer(obj.e);
							self.mousecursor
								.set({
									top: mouse.y,
									left: mouse.x
								})
								.setCoords()
								.canvas.renderAll();
						}
					});


					fabricCanvas.on('mouse:out', function (obj) {
						self.cursorIn = false;
						self.mousecursor.remove();
						// self.mousecursor
						// 	.set({
						// 		top: -100,
						// 		left: -100
						// 	})
						// 	.setCoords()
						// 	.canvas.renderAll();
					});

					self.mousecursor
						.center()
						.set({
							radius: width / 2,
							fill: color
						})
						.setCoords().canvas.renderAll();
				},
			};

			fabricCanvas.on('mouse:wheel', function(opt) {
				let delta = opt.e.deltaY;
				let zoom = self.viewer.viewport.getZoom();
				let max_zoom = self.viewer.viewport.getMaxZoom();
				let min_zoom = self.viewer.viewport.getMinZoom();
				delta = -1 * delta;
				zoom = zoom + (Math.pow(2, scroll_speed / 20) * delta) / 20000;
				if (zoom > max_zoom) zoom = max_zoom;
				if (zoom < min_zoom) zoom = min_zoom;
				let $point = new OpenSeadragon.Point(opt.e.pageX, opt.e.pageY);
				const newCenter = self.viewer.viewport.windowToViewportCoordinates($point);
				self.viewer.viewport.zoomTo(zoom, newCenter, true);
				opt.e.preventDefault();
				opt.e.stopPropagation();
			});

			$(window).on('keydown', function(e) {
				if(e.keyCode === 46) {
					fabricCanvas.remove(fabricCanvas.getActiveObject());
				}

				if(e.keyCode === 16) {
					isShiftPressed = true;
				}

				if(e.keyCode === 69) {
					$(selectors.erase_checkbox).prop('checked', !eraseMode).change();
				}
			});

			$(window).on('keyup', function(e) {
				if(e.keyCode === 16) {
					isShiftPressed = false;
				}
			});

			fabricCanvas.on('path:created', function (opt) {
				opt.path.setStroke($(selectors.color).val());
				opt.path.globalCompositeOperation = eraseMode ? 'destination-out' : 'source-over';
				fabricCanvas.renderAll();
			});
		}

		updateNewOverlayCanvas();

		// fabricCanvas.on('object:added',function(){
		// 	if(!isRedoing){
		// 		h = [];
		// 	}
		// 	isRedoing = false;
		// });

		function undo() {
			if(mouseCursor.cursorIn) {
				mouseCursor.cursorIn = false;
				mouseCursor.mousecursor.remove();
			}

			if(fabricCanvas._objects.length > 0){
				if(!h[self.layer.currentLayer]) {
					h[self.layer.currentLayer] = [];
				}
				h[self.layer.currentLayer].push(fabricCanvas._objects.pop());
				console.log(self.layer.currentLayer, h);
				fabricCanvas.renderAll();
				setCurrentMouseLastPos();
			}
		}
		function redo() {
			if(mouseCursor.cursorIn) {
				mouseCursor.cursorIn = false;
				mouseCursor.mousecursor.remove();
			}
			if(h[self.layer.currentLayer] && h[self.layer.currentLayer].length > 0){
				isRedoing = true;
				fabricCanvas.add(h[self.layer.currentLayer].pop());
				console.log(self.layer.currentLayer, h);
				setCurrentMouseLastPos();
			}
		}

		function hexToRgbA(hex){
			let c;
			if(/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)){
				c= hex.substring(1).split('');
				if(c.length === 3){
					c= [c[0], c[0], c[1], c[1], c[2], c[2]];
				}
				c= '0x'+c.join('');
				return 'rgba('+[(c>>16)&255, (c>>8)&255, c&255].join(',')+',0.5)';
			}
			throw new Error('Bad Hex "' + hex + '"');
		}

		/////undo-redo/////

		let setCurrentMouseLastPos = function() {
			let objects = fabricCanvas._objects;
			if(objects && objects[objects.length - 1]) {
				let object = objects[objects.length - 1];
				if(object._objects && object._objects.length) {
					object = object._objects[1];
				}

				if(object.path) {
					let pathLastPoint = object.path[object.path.length - 1];
					mouseLastPosition = {x: pathLastPoint[1], y: pathLastPoint[2]};
				}
			}
		};

		////!undo-redo!////

		$(selectors.undo).on('click', function() {
			undo();
		});

		$(selectors.redo).on('click', function() {
			redo();
		});

		$(window).on('keydown', function(e) {
			if(e.which === 90 && e.ctrlKey){
				undo();
			}

			if(e.which === 89 && e.ctrlKey){
				redo();
			}
		});

		let checkSketchMode = function() {
			setSketchOptions();
			self.viewer.setMouseNavEnabled(false);
			self.viewer.outerTracker.setTracking(false);
			currentLayerOverlay.fabricCanvas().isDrawingMode = true;
		};

		let width;
		let color;
		let setSketchOptions = function () {
			let c = hexToRgbA($(selectors.color).val());
			// let c = $(selectors.color).val();
			currentLayerOverlay.fabricCanvas().freeDrawingBrush.width = parseInt($(selectors.size).val());
			currentLayerOverlay.fabricCanvas().freeDrawingBrush.color = eraseMode ? 'rgba(255,255,255)' : c;
			width = parseInt($(selectors.size).val());
			color = eraseMode ? 'rgb(255,255,255)' : c;
			if(mouseCursor.mousecursor) {
				let mouseCursorSett = mouseCursor.mousecursor
					.center()
					.set({
						radius: width / 2,
						fill: color,
					})
					.setCoords();

				if(mouseCursorSett.canvas) {
					mouseCursorSett.canvas.renderAll();
				}
			}
		};

		$(selectors.erase_checkbox).on('change', function () {
			eraseMode = $(this).is(':checked');
			setSketchOptions();
		});

		$(selectors.poly_checkbox).on('change', function () {
			polyMode = $(this).is(':checked');

			if(polyMode) {
				self.layer.currentLayerOverlay.fabricCanvas().forEachObject(function(obj) {
					polyObject(obj);
				});
				self.layer.currentLayerOverlay.fabricCanvas().isDrawingMode = false;
			} else {
				disableAllPolyShadows();
				self.layer.currentLayerOverlay.fabricCanvas().isDrawingMode = true;
			}
		});

		let polyObject = function(object) {
			if(object.selectable) {
				object.selectable = false;
				object.on('mousedown', function() {
					if(!object.getShadow()) {
						object.setShadow('-1px 0px 100px rgba(255,0,0,1)');
					} else {
						object.setShadow(null);
					}
				});
			}
		};

		let shapePoly = function() {
			let points = [];
			let objects = [];
			self.layer.currentLayerOverlay.fabricCanvas().forEachObject(function(object) {
				if(object && object.getShadow()) {
					if(object.type === 'group') {
						let obj = object._objects;
						points = points.concat(pushShapePolyCoors(obj[0]));
						points = points.concat(pushShapePolyCoors(obj[1]));
					} else {
						points = points.concat(pushShapePolyCoors(object));
					}
					objects.push(object);
				}
			});

			$.each(objects, function(key, ob) {
				ob.remove();
			});

			let polyCenter = findPolyCenter(points);
			findPolyAngles(polyCenter, points);

			points.sort(function(a, b) {
				return (a.angle >= b.angle) ? 1 : -1
			});

			let polygon = new fabric.Polygon(points, {
				fill: $(selectors.color).val(),
				selectable: false,
				objectCaching: false,
			});
			self.layer.currentLayerOverlay.fabricCanvas().add(polygon);
		};

		let findPolyCenter = function(points) {
			let x = 0, y = 0, i, len = points.length;
			for (i = 0; i < len; i++) {
				x += points[i].x;
				y += points[i].y;
			}
			return {x: x / len, y: y / len};
		};

		let findPolyAngles = function (c, points) {
			let i, len = points.length, p, dx, dy;
			for (i = 0; i < len; i++) {
				p = points[i];
				dx = p.x - c.x;
				dy = p.y - c.y;
				p.angle = Math.atan2(dy, dx);
			}
		};

		let pushShapePolyCoors = function(obj) {
			return [
				{x: obj.path[0][1], y: obj.path[0][2], angle: 0},
				{x: obj.path[obj.path.length - 1][1], y: obj.path[obj.path.length - 1][2], angle: 0},
			]
		};

		$(window).on('keydown', function(e) {
			if(e.keyCode === 13 && polyMode) {
				shapePoly();
			}

		});

		let disableAllPolyShadows = function() {
			$.each(self.layer.layers, function (layerName, layer) {
				layer.fabricCanvas().forEachObject(function(obj) {
					obj.setShadow(null);
				});
				layer.fabricCanvas().renderAll();
			});
		};

		checkSketchMode();

		document.oncontextmenu = function() {return false;};

		$(window).on('mousedown',function (event) {
			if(event.which === 1) {
				self.MouseTracker.setTracking(false);

			}
		});

		$(window).on('mouseup',function (event) {
			if(event.which === 1) {
				self.MouseTracker.setTracking(true);
			}
		});

		let scroll_speed = $(selectors.scroll).val() ? $(selectors.scroll).val() : 20;
		let brush_size = $(selectors.size).val() ? $(selectors.size).val() : 20;
		scroll_speed = (rel = $.cookie('scroll_speed')) ? rel : scroll_speed;
		brush_size = (rel = $.cookie('brush_size')) ? rel : brush_size;

		if(scroll_speed >= 50 && scroll_speed <= 200) {
			$(selectors.scroll).val(scroll_speed).change();
		}

		if(brush_size >= self.brushMinSize && brush_size <= self.brushMaxSize) {
			$(selectors.size).val(brush_size).change();
		}

		/*save json*/

		$(selectors.save_bt).on('click', function() {
			disableAllPolyShadows();
			if(isNewFabric) {
				saveJson(svs_name, $(selectors.fabric_name).val(), JSON.stringify(self.layer.currentLayerOverlay.fabricCanvas()), isNewFabric);
			} else {
				$.each(self.layer.layers, function(fabric_name, fabric_data) {
					saveJson(svs_name, fabric_name, JSON.stringify(fabric_data.fabricCanvas()));
				});
			}
		});

		let saveJson = function(svs_name, fabric_name, fabric_data, fabric_new) {
			$.post(urls.save_url, {svs_name: svs_name, fabric_name: fabric_name, data: fabric_data}, function(data) {
				if(!data.success) {
					$.growl.error({ title: 'Ошибка', message: data.message });
				} else {
					if(fabric_new) {
						location.href = urls.page_url + '?svs_name=' + svs_name + '&fabric_name=' + fabric_name;
					}
					$.growl.notice({ title: 'Успех', message: data.message });
				}
			});
		};

		/*!save-json!*/

		Array.prototype.next = function() {
			return this[++this.current];
		};
		Array.prototype.prev = function() {
			return this[--this.current];
		};
		Array.prototype.current = 0;
		/*!load fabric!*/

		/*change layer checkbox*/
		if(!isNewFabric) {
			self.layer.$checkboxUlElement.find('.layer-checkbox').on('change', function () {
				self.layer.updateParams();
				currentLayerOverlay = self.layer.currentLayerOverlay;
				checkSketchMode();
				updateNewOverlayCanvas();
				mouseCursor.init();
			});
		}
		/*!change layer checkbox!*/

		/*change svs*/
		$(selectors.svs_select).on('change', function () {
			let svs = $(this).val();
			location.href = urls.page_url + '?svs_name=' + svs;
		});
		/*!change svs!*/

		/*size-range*/
		$(selectors.size).on('change', function () {
			let size_range = parseInt($(this).val());
			size_range = size_range < self.brushMinSize ? self.brushMinSize : size_range;
			size_range = size_range > self.brushMaxSize ? self.brushMaxSize : size_range;
			$(selectors.size_range).val(size_range);
			$(this).val(size_range);
			$.cookie('brush_size', size_range);
		});

		$(selectors.size_range).on('input', function () {
			let size_range = parseInt($(this).val());
			size_range = size_range < self.brushMinSize ? self.brushMinSize : size_range;
			size_range = size_range > self.brushMaxSize ? self.brushMaxSize : size_range;
			$(selectors.size).val(size_range);
			$(selectors.size).change();
			$(this).val(size_range);
		});
		/*!size-range!*/

		/*scroll-range*/
		$(selectors.scroll).on('change', function () {
			let scroll_range = parseInt($(this).val());
			scroll_range = scroll_range < 50 ? 50 : scroll_range;
			scroll_range = scroll_range > 200 ? 200 : scroll_range;
			$(selectors.scroll_range).val(scroll_range);
			$(this).val(scroll_range);
			scroll_speed = scroll_range;
			$.cookie('scroll_speed', scroll_speed);
		});

		$(selectors.scroll_range).on('input', function () {
			let scroll_range = parseInt($(this).val());
			scroll_range = scroll_range < 50 ? 50 : scroll_range;
			scroll_range = scroll_range > 200 ? 200 : scroll_range;
			$(selectors.scroll).val(scroll_range);
			$(selectors.scroll).change();
			$(this).val(scroll_range);
		});
		/*!scroll-range!*/

		/*select2*/
		$(selectors.fabrics_select).select2({
			width: '100%',
			placeholder: "",
			allowClear: true,
			maximumSelectionLength: 4
		});
		/*!select2!*/

		/*save fabric*/
		$(selectors.fabric_append_select).on('click', function() {
			let fabric_selected = $(selectors.fabrics_select).val();
			let model_selected = $(selectors.model_select).val();
			fabric_selected = fabric_selected === null ? '' : fabric_selected;
			location.href = urls.page_url + '?svs_name=' + svs_name + '&fabric_name=' + fabric_selected + '&model=' + model_selected;
		});
		/*!save fabric!*/

		mouseCursor.init();
		return this;
	},
	layer: {
		General: this,
		currentLayer: null,
		layers: {},
		currentLayerOverlay: null,
		$checkboxUlElement: null,
		showLayerCheckboxes: function() {
			this.$checkboxUlElement = $('<ul class="layer-list"></ul>');
			$(this.General.App.selectors.layers_list).append(this.$checkboxUlElement);
			let self = this;
			let iter = 0;
			$.each(this.layers, function(name, layer) {
				self.$checkboxUlElement.append($('<li><i class="fa fa-arrows-alt icon-move"></i>  <input type="checkbox" class="layer-checkbox" data-name="' + name + '" ' + ((iter++) === 0 ? 'checked="checked"' : '') + '> ' + name + '</li>'));
			});

			self.$checkboxUlElement.sortable({
				group: 'no-drop',
				handle: 'i.icon-move',
				onDrop: function ($item, container, _super, event) {
					self.$checkboxUlElement.find('input[type="checkbox"]').first().change();
					$item.removeClass(container.group.options.draggedClass).removeAttr("style");
					$("body").removeClass(container.group.options.bodyClass);
				},
			});
		},
		addLayer: function(name, fabric_json) {
			let overlay = self.App.viewer.fabricjsOverlay(self.App.viewerOptions);
			$(overlay._canvas).css('opacity', '0.5')
			if(!Object.keys(this.layers).length) {
				this.currentLayer = name;
				this.currentLayerOverlay = overlay;
			}
			overlay.fabricCanvas().loadFromJSON(fabric_json);
			this.layers[name] = overlay;
			return overlay;
		},
		newLayer: function() {
			let overlay = self.App.viewer.fabricjsOverlay(self.App.viewerOptions);
			$(overlay._canvas).css('opacity', '0.5')
			this.currentLayer = name;
			this.currentLayerOverlay = overlay;
			this.layers[name] = overlay;
			return overlay;
		},
		resize: function () {
			$.each(this.layers, function(name, layer) {
				$(window).resize(function() {
					layer.resize();
					layer.resizecanvas();
				})
			})
		},
		updateParams: function() {
			let self = this;
			let iter = 4;
			let isCheckedDraw = false;
			this.$checkboxUlElement.find('input[type="checkbox"]').each(function() {
				let layer = self.layers[$(this).data('name')];
				if($(this).prop('checked')) {
					if(!isCheckedDraw) {
						self.currentLayer = $(this).data('name');
						self.currentLayerOverlay = self.layers[self.currentLayer];
						isCheckedDraw = true;
					}
					$(layer._canvas).show();
				} else {
					$(layer._canvas).hide();
				}

				$(layer._canvas).parent().parent().css('z-index', $(this).prop('checked') ? iter * 100 : 0);
				iter--;
				layer.fabricCanvas().isDrawingMode = false;
			});
		}
	},
	getSimpleColor: function() {
		let self = this;
		$(self.selectors.color).simplecolorpicker();

		$(window).on('keydown', function(e) {
			if (e.keyCode === 76) {
				$spanColorPicker = $('span.simplecolorpicker');
				$nextSCP = $spanColorPicker.find('span[data-selected]').next();

				if ($nextSCP.length) {
					$nextSCP.click();
				} else {
					$spanColorPicker.find('span:first').click();
				}
			}
		});
	},
	showViewElements: function() {
		let $panelInfo = $('<div class="panel info-panel">\n' +
			'\t\t\t<div>\n' +
			'\t\t\t\t<div style="position: relative;"><h3>Information</h3> <span class="fa fa-angle-down angle-info-panel"></span></div> \n' +
			'\t\t\t\t<div class="info-panel-body"><p>Draw with marker - Left mouse button</p>\n' +
			'\t\t\t\t<p>Move Image - Right Mouse Button</p>\n' +
			'\t\t\t\t<p>Enlarge Image - Mouse Scroll</p>\n' +
			// '\t\t\t\t<p>Change image - letter "j"</p></div> \n' +
			'\t\t\t</div>\n' +
			'\t\t</div>');
		$(this.selectors.viewer_container).append($panelInfo);

		let $panelInfoBody = $panelInfo.find('.info-panel-body');
		let $panelInfoAngle = $panelInfo.find('.angle-info-panel');
		$panelInfo.find('.angle-info-panel').on('click', function () {
			if($panelInfoBody.is(':hidden')) {
				$panelInfoBody.slideDown();
				$panelInfoAngle.removeClass('fa-angle-down');
				$panelInfoAngle.addClass('fa-angle-up');
			} else {
				$panelInfoBody.slideUp();
				$panelInfoAngle.removeClass('fa-angle-up');
				$panelInfoAngle.addClass('fa-angle-down');
			}
		});
	},
	setViewElementsProperties: function() {
		let sopWidth = ($(this.selectors.sketch_options_panel).width() + 50);
		let viewer_width = 'calc(100% - ' + sopWidth + 'px)';
		$('#' + this.viewerId).css('width', viewer_width);
		$('#' + this.viewerIdP).css('width', viewer_width);
		$('#' + this.viewerEmpty).css('width', viewer_width);
		$(this.selectors.undo_redo_select_block).css('width', viewer_width);

		let self = this;

		if(self.selectors.export_xml_bt && $(self.selectors.export_xml_bt).length) {
			$(self.selectors.export_xml_bt).on('click', function() {
				loadingOverlay('show', false);
				let fabricName = self.layer.currentLayer;
				let svs_name = self.svs_name;

				$.ajax({
					url: self.urls.export_xml_url,
					type: "POST",
					data: {fabric_name: fabricName, svs_name: svs_name, fabric_json: JSON.stringify(self.layer.currentLayerOverlay.fabricCanvas())},
					success: function(data, status, xhr) {
						if(data.success && data.name && data.xml) {
							var type = 'text/xml';
							var blob = new Blob([data.xml], { type: type });
							var a = document.createElement('a');
							var url = window.URL.createObjectURL(blob);
							a.href = url;
							a.download = data.name + '.xml';
							document.body.append(a);
							a.click();
							a.remove();
							window.URL.revokeObjectURL(url);
							loadingOverlay('hide');
						} else if(!data.success) {
							$.growl.error({ title: 'Error', message: data.message });
							loadingOverlay('hide');
						}
					},
					error: function(data) {
						$.growl.error({ title: 'Error', message: 'An error occurs' });
						loadingOverlay('hide');
					}
				});
			})
		}

		if(self.selectors.select_download_xml_file_bt && $(self.selectors.select_download_xml_file_bt).length) {
			$(self.selectors.select_download_xml_file_bt).on('click', function () {
				$(self.selectors.select_download_xml_file_input).click();
			});

			let file_download_bt_selector = '#file-download-xml-bt';
			$(self.selectors.select_download_xml_file_input).on('change', function(e) {
				let filename = e.target.files[0].name;

				let filename_ob = $('#download-xml-filename');

				if(filename_ob.length) {
					filename_ob.text('Selected: ' + filename);
				} else {
					filename_ob = $('<p id="download-xml-filename">Selected: ' + filename + '</p>');
					$(self.selectors.select_download_xml_file_input).before(filename_ob);
				}

				if(filename) {
					if(!$(file_download_bt_selector).length) {
						$(self.selectors.select_download_xml_file_bt).before($('<p class="btn btn-success col-xs-12" id="' + file_download_bt_selector.replace('#','') + '" style="margin-bottom: 5px;">Import</p>'))
					}
				} else {
					$(self.selectors.select_download_xml_file_bt).remove();
				}
			})

			$(document).delegate(file_download_bt_selector, 'click', function() {
				$(self.selectors.select_download_xml_file_form).submit();
			})

			$(document).delegate(self.selectors.select_download_xml_file_form, 'submit', function(e) {
				if (!e.isDefaultPrevented()) {
					e.preventDefault();
				}

				loadingOverlay('show', false);

				$.ajax({
					url: "/view/download_xml_file.php?svs_name=" + self.svs_name,
					type: "POST",
					data: new FormData($(self.selectors.select_download_xml_file_form)[0]),
					contentType: false,
					processData: false,
					dataType: 'json',
					success: function(data) {
						if(data.success && data.name) {
							location.href = self.urls.page_url + '?svs_name=' + self.svs_name + '&fabric_name=' + data.name;
						} else if(!data.success) {
							$.growl.error({ title: 'Error', message: data.message });
							loadingOverlay('hide');
						}
					},
					error: function(data) {
						$.growl.error({ title: 'Error', message: 'An error occurs' });
						loadingOverlay('hide');
					}
				});

				return false;
			})
		}

		if(self.selectors.select_download_file_bt && $(self.selectors.select_download_file_bt).length) {
			$(self.selectors.select_download_file_bt).on('click', function() {
				$(self.selectors.select_download_file_input).click();
			});

			let file_download_bt_selector = '#file-download-bt';
			$(self.selectors.select_download_file_input).on('change', function(e) {
				let filename = e.target.files[0].name;

				let filename_ob = $('#download-filename');

				if(filename_ob.length) {
					filename_ob.text('Selected: ' + filename);
				} else {
					filename_ob = $('<p id="download-filename">Selected: ' + filename + '</p>');
					$(self.selectors.select_download_file_input).before(filename_ob);
				}

				if(filename) {
					if(!$(file_download_bt_selector).length) {
						$(self.selectors.select_download_file_bt).before($('<p class="btn btn-success col-xs-12" id="' + file_download_bt_selector.replace('#','') + '" style="margin-bottom: 5px;">Upload</p>'))
					}
				} else {
					$(self.selectors.select_download_file_bt).remove();
				}
			})

			$(document).delegate(file_download_bt_selector, 'click', function() {
				$(self.selectors.select_download_file_form).submit();
			})

			$(document).delegate(self.selectors.select_download_file_form, 'submit', function(e) {
				if (!e.isDefaultPrevented()) {
					e.preventDefault();
				}

				loadingOverlay('show', true, 'Upload process');

				$.ajax({
					url: "/view/download_file.php",
					type: "POST",
					data: new FormData($(self.selectors.select_download_file_form)[0]),
					contentType: false,
					processData: false,
					dataType: 'json',
					xhr        : function ()
					{
						var jqXHR = null;
						if ( window.ActiveXObject )
						{
							jqXHR = new window.ActiveXObject( "Microsoft.XMLHTTP" );
						}
						else
						{
							jqXHR = new window.XMLHttpRequest();
						}
						//Upload progress
						jqXHR.upload.addEventListener( "progress", function ( evt )
						{
							if ( evt.lengthComputable )
							{
								var percentComplete = Math.round( (evt.loaded * 100) / evt.total );
								//Do something with upload progress
								console.log( 'Uploaded percent', percentComplete );
								loadingOverlaySetPercent(percentComplete);
							}
						}, false );
						//Download progress
						jqXHR.addEventListener( "progress", function ( evt )
						{
							if ( evt.lengthComputable )
							{
								var percentComplete = Math.round( (evt.loaded * 100) / evt.total );
								//Do something with download progress
								console.log( 'Downloaded percent', percentComplete );
								loadingOverlaySetPercent(percentComplete);
							}
						}, false );
						return jqXHR;
					},
					success: function(data) {
						if(data.delete_modal && !data.result && !data.size) {
							$.growl.error({ title: 'Error', message: 'no disk space' });
							loadingOverlay('hide');
						} else if(data.success && data.name) {
							loadPyramidPB(data.full_name, data.name)
						} else if(!data.success) {
							$.growl.error({ title: 'Error', message: data.message });
							loadingOverlay('hide');
						}
					},
					error: function(data) {
						$.growl.error({ title: 'Error', message: 'An error occurs' });
						loadingOverlay('hide');
					}
				});

				return false;
			})
		}

		let loadPyramidPB = function(full_name, name) {
			let intervalId = null;
			let currPercent = '0%';

			loadingOverlaySetText('Pyramidization process');
			$.ajax({
				url: "/view/pyramid_file.php",
				type: "POST",
				data: {name : full_name},
				success: function(data) {
					if(data.success) {
						location.href = self.urls.page_url + '?svs_name=' + name;
					} else if(!data.success) {
						$.growl.error({ title: 'Error', message: data.message });
						clearInterval(intervalId);
						loadingOverlay('hide');
					}
				},
				error: function(data) {
					$.growl.error({ title: 'Error', message: 'An error occurs' });
					clearInterval(intervalId);
					loadingOverlay('hide');
				}
			});

			let startPercenting = function() {
				intervalId = setInterval(function() {
					$.get('/svs_library/' + name + '/progress.txt', {"_": $.now()}, function(response) {
						let percents = response.match(/(\d+%)/g);
						if(percents) {
							currPercent = percents.slice(-1).pop();
							currPercent = currPercent.replace('%', '');
							loadingOverlaySetPercent(currPercent);
						}
					})

					if(currPercent == '100') {
						stopInterval();
					}
				}, 3000);

				let stopInterval = function() {
					clearInterval(intervalId);
				}
			}
			startPercenting();
		}

		let loadingOverlay = function(method, show_percent = true, text = '') {
			if(method === 'show') {
				$.LoadingOverlay("show");

				if(show_percent) {
					$lo_pb = $('lo_pb');
					if($lo_pb.length) {
						$('.pb-num').text('0');
					}

					$('.loadingoverlay').append($('<p class="lo_pb"><span class="pb-num">0</span>%</p>'));
				}

				if(text) {
					$('.loadingoverlay').append($('<p class="lo_text_pb">' + text + '</p>'));
				}
			} else if(method === 'hide') {
				$.LoadingOverlay("hide");
			}
		};

		let loadingOverlaySetText = function(text) {
			$('.loadingoverlay .lo_text_pb').text(text);
		}

		let loadingOverlaySetPercent = function(percent) {
			$('.loadingoverlay .lo_pb .pb-num').text(percent);
		};
	}
};